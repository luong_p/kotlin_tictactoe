package Adapter

import Model.Model
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import viewHolder.MyViewHolderScoreTable
import com.example.paulluong.kotlin_test.R
import java.text.FieldPosition
import java.util.*

class MyAdapterScoreTable (internal var list: ArrayList<Model.ScoreTable>?,
                           internal var context: Context)
                           : RecyclerView.Adapter<MyViewHolderScoreTable>(){


    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MyViewHolderScoreTable? {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.score_table_card, parent, false)
        return MyViewHolderScoreTable(view)
    }

    override fun onBindViewHolder(holder: MyViewHolderScoreTable, position: Int) {
        var scoreTable = list?.get(position)
        holder.bind(scoreTable!!)
    }

}