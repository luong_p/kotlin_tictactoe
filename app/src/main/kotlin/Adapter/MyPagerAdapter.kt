package Adapter

import android.support.v4.app.FragmentManager
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentPagerAdapter
import ui.LoginFragment
import ui.ScoreTableFragment
import ui.TicTacToeFragment
import java.text.FieldPosition

class MyPagerAdapter (fm : FragmentManager) : FragmentPagerAdapter(fm) {

    var NUM_FRAGMENT : Int = 3;

    override fun getItem(position : Int): Fragment? {
        when(position){
            0-> return LoginFragment()
            1-> return TicTacToeFragment()
            2-> return ScoreTableFragment()
            else-> return null
        }

    }


    override fun getCount(): Int {
       return  NUM_FRAGMENT;
    }


}