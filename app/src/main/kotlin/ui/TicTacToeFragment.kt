package ui

import TicTacToeClasses.TicTacToe
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.example.paulluong.kotlin_test.R
import kotlinx.android.synthetic.main.fragment_tic_tac_toe.*
import java.util.*


class TicTacToeFragment : Fragment() {

    private var curTurn: Char = '\u0000'
    var BoardButton = ArrayList<Button>()
    var TurnTextView: TextView? = null
    var ticTacToe: TicTacToe? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view: View = inflater!!.inflate(R.layout.fragment_tic_tac_toe, container, false)
        TurnTextView = view.findViewById(R.id.TicTacToe_TurnTextView) as TextView
        ticTacToe = TicTacToe(BoardButton, activity)
        curTurn = 'O'
        FillButtonArray(view)
        clearBoard()
        writeTurn(curTurn)
        var ResetButton:Button = view.findViewById(R.id.TicTacToe_ResetButton) as Button
        ResetButton.setOnClickListener( object : View.OnClickListener {
            override fun onClick(p0: View?) {
                Toast.makeText(activity, "Reset", Toast.LENGTH_SHORT).show()
                (ticTacToe as TicTacToe).clearBoard()
            }

        })
        return view
    }

    fun FillButtonArray(view: View) {
        BoardButton.add(view.findViewById(R.id.TicTacToe_one) as Button);
        BoardButton.add(view.findViewById(R.id.TicTacToe_two) as Button);
        BoardButton.add(view.findViewById(R.id.TicTacToe_three) as Button);
        BoardButton.add(view.findViewById(R.id.TicTacToe_four) as Button);
        BoardButton.add(view.findViewById(R.id.TicTacToe_five) as Button);
        BoardButton.add(view.findViewById(R.id.TicTacToe_six) as Button);
        BoardButton.add(view.findViewById(R.id.TicTacToe_seven) as Button);
        BoardButton.add(view.findViewById(R.id.TicTacToe_eight) as Button);
        BoardButton.add(view.findViewById(R.id.TicTacToe_nine) as Button);
    }

    fun clearBoard() {
        for (i in BoardButton.indices) {
            BoardButton[i].text = ""
            BoardButton[i].isEnabled = true
            BoardButton[i].setOnClickListener(ButtonClickListener(i))
        }
    }

    inner class ButtonClickListener(internal var location: Int) : View.OnClickListener {

        override fun onClick(v: View) {
            if (BoardButton[location].isEnabled) {

                check_turn()

                ticTacToe?.setTill(location, curTurn)
                ticTacToe?.setBoardValue(location, curTurn)
                ticTacToe?.checkWinner(curTurn)
                writeTurn(curTurn)
            }
        }
    }

    fun writeTurn(curTurn: Char) {
        if (curTurn == 'X')
            TurnTextView?.text = 'O' + " player to play "
        else
            TurnTextView?.text = 'X' + " player to play "
    }

    fun check_turn() {
        if (curTurn == 'X')
            curTurn = 'O'
        else
            curTurn = 'X'
    }

}

