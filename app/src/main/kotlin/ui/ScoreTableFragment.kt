package ui

import Adapter.MyAdapterScoreTable
import InterfaceAPI.getScoreTableInterface
import Model.Model
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import butterknife.bindView
import com.example.paulluong.kotlin_test.R
import retrofit.Callback
import retrofit.RestAdapter
import retrofit.RetrofitError
import retrofit.client.Response
import java.util.*


class ScoreTableFragment : Fragment() {

    val API_BASE_URL = "http://www.sur-le-web.net"
    var scoreTable = ArrayList<Model.ScoreTable>()
    var recyclerView: RecyclerView? = null//by bindView(R.id.ScoreFragmentRecyclerView)



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        val view: View = inflater!!.inflate(R.layout.fragment_score, container, false)
        recyclerView = view.findViewById(R.id.ScoreFragmentRecyclerView) as RecyclerView?
        getScore()
        return view
    }


    fun getScore() {
        var scoreTableService: getScoreTableInterface
        val adapter = RestAdapter.Builder()
                        .setEndpoint(API_BASE_URL)
                        .build()
        scoreTableService = adapter.create(getScoreTableInterface::class.java)

        scoreTableService.getScore(object : Callback<ArrayList<Model.ScoreTable>> {
            override fun success(ListscoreTable: ArrayList<Model.ScoreTable>?, response: Response?) {

                recyclerView?.layoutManager = LinearLayoutManager(activity)
                recyclerView?.adapter = MyAdapterScoreTable(ListscoreTable, activity)
            }

            override fun failure(p0: RetrofitError?) {
                Toast.makeText(activity, "" + p0, Toast.LENGTH_SHORT).show()
            }
        });

    }
}


