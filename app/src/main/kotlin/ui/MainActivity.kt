package ui

import Adapter.MyPagerAdapter
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentPagerAdapter
import com.example.paulluong.kotlin_test.R
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        var adapterViewPager = MyPagerAdapter(supportFragmentManager) as FragmentPagerAdapter
        ViewPager_main_activity.setAdapter(adapterViewPager)


        /*supportFragmentManager
                .beginTransaction()
                .replace(R.id.main_container, LoginFragment())
                .commit()
        */
    }
}

