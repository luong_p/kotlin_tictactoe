package viewHolder


import Model.Model
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import butterknife.bindView
import com.example.paulluong.kotlin_test.R
import kotlinx.android.synthetic.main.score_table_card.*
import java.util.*



public class MyViewHolderScoreTable (itemView: View): RecyclerView.ViewHolder(itemView) {

    val firstname: TextView by bindView(R.id.scoreTableFirstNameTextView)
    val lastname : TextView by bindView (R.id.scoreTableLastNameTextView)
    val score : TextView by bindView(R.id.scoreTableScoreTextView)
    val Id : TextView by bindView(R.id.scoreTableIdTextView)
/*
    val firstname: TextView = itemView.findViewById(R.id.scoreTableFirstNameTextView) as TextView
    val lastname : TextView  = itemView.findViewById(R.id.scoreTableLastNameTextView) as TextView
    val score : TextView = itemView.findViewById(R.id.scoreTableScoreTextView) as TextView
    val Id : TextView  = itemView.findViewById(R.id.scoreTableIdTextView) as TextView
*/
    fun bind (obj : Model.ScoreTable) {
        firstname.text = "" + obj.player_firstname
        lastname.text =  "" + obj.player_lastname
        score.text = "" + obj.result
        Id.text = "" + obj.id

    }

}