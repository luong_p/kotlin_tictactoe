package InterfaceAPI




import Model.Model
import retrofit.Callback
import retrofit.http.GET
import java.util.*

interface getScoreTableInterface{
    @GET("/data/scores.json")
    fun getScore (reponse: retrofit.Callback<ArrayList<Model.ScoreTable>>)
}