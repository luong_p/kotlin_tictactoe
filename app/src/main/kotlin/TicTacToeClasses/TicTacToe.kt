package TicTacToeClasses

import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.support.v7.app.AlertDialog
import android.widget.Button
import ui.TicTacToeFragment
import java.util.*

class TicTacToe(val BoardButton: ArrayList<Button>, val context: Context) {


    internal var ARRAY_SIZE: Int = 3
    var board = Array(3, { CharArray(3) })
    internal var popup: AlertDialog? = null

    init {
        clearBoard()
        val popupBuilder = AlertDialog.Builder(context)
        popupBuilder.setTitle("The winner")
        popupBuilder.setNeutralButton("Replay") { dialog, which -> clearBoard() }
        popup = popupBuilder.create()
        popup?.setCancelable(false)
    }

    /*
        var BoardButton: Array<Button>
        internal var board: Array<CharArray>
        internal var context: Context

        internal var popup: AlertDialog


        constructor(boardbButton: Array<Button>, context: Context) {
            this.context = context
            BoardButton = boardbButton
            board = Array(ARRAY_SIZE) { CharArray(ARRAY_SIZE) }
            val popupBuilder = AlertDialog.Builder(context)
            popupBuilder.setTitle("The winner")
            popupBuilder.setNeutralButton("Replay") { dialog, which -> clearBoard() }
            popup = popupBuilder.create()
            popup.setCancelable(false)
        }

        constructor() {
        }
        */
    fun setTill(location: Int, tillValue: Char): Char {
        BoardButton[location].isEnabled = false
        BoardButton[location].text = "" + tillValue
        if (tillValue == 'X')
            BoardButton[location].setTextColor(Color.RED)
        else
            BoardButton[location].setTextColor(Color.BLUE)
        return tillValue

    }

    fun clearBoard() {
        for (l in BoardButton.indices) {
            BoardButton[l].text = ""
            BoardButton[l].isEnabled = true
        }
        for (i in 0..ARRAY_SIZE - 1) {
            for (j in 0..ARRAY_SIZE - 1) {
                board[i][j] = ' '
            }
        }
    }

    //set board array value
    fun setBoardValue(positon: Int, type: Char) {
        if (positon == 0) {
            board[0][0] = type
        }
        if (positon == 1) {
            board[0][1] = type
        }
        if (positon == 2) {
            board[0][2] = type
        }
        if (positon == 3) {
            board[1][0] = type
        }
        if (positon == 4) {
            board[1][1] = type
        }
        if (positon == 5) {
            board[1][2] = type
        }
        if (positon == 6) {
            board[2][0] = type
        }
        if (positon == 7) {
            board[2][1] = type
        }
        if (positon == 8) {
            board[2][2] = type
        }
    }

    fun checkWinner(player: Char) {
        //line
        for (x in 0..ARRAY_SIZE - 1) {
            var total = 0
            for (y in 0..ARRAY_SIZE - 1) {
                if (board[x][y] == player) {
                    total++
                }
            }
            if (total == 3) {
                popup?.setMessage("The winner is the $player player !")
                popup?.show()
                /* Toast.makeText(context, "LINE the winner is " + player, Toast.LENGTH_SHORT).show();
                clearBoard();*/
            }
        }
        //columm

        for (x in 0..ARRAY_SIZE - 1) {
            var total = 0
            for (y in 0..ARRAY_SIZE - 1) {
                if (board[y][x] == player) {
                    total++
                }
            }
            if (total == 3) {
                popup?.setMessage("The winner is the $player player !")
                popup?.show()
                /* Toast.makeText(context, "LINE the winner is " + player, Toast.LENGTH_SHORT).show();
                clearBoard();*/
            }
        }
        //diagonal 1
        var total = 0
        for (x in 0..ARRAY_SIZE - 1) {
            for (y in 0..ARRAY_SIZE - 1) {
                if (x == y && board[x][y] == player) {
                    total++
                }
            }
        }
        if (total == ARRAY_SIZE) {
            popup?.setMessage("The winner is the $player player !")
            popup?.show()
            /* Toast.makeText(context, "LINE the winner is " + player, Toast.LENGTH_SHORT).show();
                clearBoard();*/
        }

        // backward diag
        total = 0
        for (x in 0..ARRAY_SIZE - 1) {
            for (y in 0..ARRAY_SIZE - 1) {
                if (x + y == ARRAY_SIZE - 1 && board[x][y] == player) {
                    total++
                }
            }
        }
        if (total == ARRAY_SIZE) {
            popup?.setMessage("The winner is the $player player !")
            popup?.show()
            /* Toast.makeText(context, "LINE the winner is " + player, Toast.LENGTH_SHORT).show();
                clearBoard();*/
        }
    }

}
