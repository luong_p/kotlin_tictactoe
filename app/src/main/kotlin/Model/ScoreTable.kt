package Model



object Model {

    data class  ScoreTable(var id: Int,
                           var player_firstname: String?,
                           var player_lastname: String?,
                           var game_date: String?,
                           var game: Int,
                           var result: Int)
}